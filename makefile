# Qt Path - My installation of QT is: version 5.11.0 for GCC 64 bits, the complete path to libs, execs and headers looks like: /home/ayak973/qt/5.11.0/gcc_64/
QT_PATH            = /home/ayak973/qt
QT_VERSION         = 5.11.0
QT_PLATFORM        = gcc_64

# To create a build directory, to not pollute project files
DISTDIR            = build

# Output file name
DISTNAME           = midigaming

# Includes directories
INCDIRS            = -I$(QT_PATH)/$(QT_VERSION)/$(QT_PLATFORM)/include
INCDIRS           += -I$(QT_PATH)/$(QT_VERSION)/$(QT_PLATFORM)/include/QtCore
INCDIRS           += -I$(QT_PATH)/$(QT_VERSION)/$(QT_PLATFORM)/include/QtWidgets
INCDIRS           += -I$(QT_PATH)/$(QT_VERSION)/$(QT_PLATFORM)/include/QtGui

# Library dir and modules
LIBDIR             = -L$(QT_PATH)/$(QT_VERSION)/$(QT_PLATFORM)/lib
LIBS               = -lQt5Gui 
LIBS              += -lQt5Core 
LIBS              += -lQt5Widgets 
LIBS              += -lpthread 

# Tools path
CXX                = g++
UIC_EXEC           = $(QT_PATH)/$(QT_VERSION)/$(QT_PLATFORM)/bin/uic
MOC_EXEC           = $(QT_PATH)/$(QT_VERSION)/$(QT_PLATFORM)/bin/moc
COPY_EXEC          = cp -f
MOVE_EXEC          = mv -f
MKDIR_EXEC         = mkdir -p
TEST_EXEC          = @test -d
ECHO_EXEC          = @echo
RM_EXEC            = rm -rf


# Qt Community need this option when downloaded from the official web installer (qt was compiled with "position independent code" option)
QT_OPT             = -fPIC

# C++ flags
CXXFLAGS           = -c -Wall -std=c++14

# Souces files (copy all *.cpp files in current dir, not following subdirs)
SOURCES           := $(notdir $(shell find . -maxdepth 1 -name '*.cpp'))

# Ui files to be Uic'ed (copy all *.cpp files in current dir, not following subdirs)
UI_FILES:=$(notdir $(shell find . -maxdepth 1 -name '*.ui'))

# Headers who need to be moc'ed (classes defined inside .h containing Q_OBJECT macro)
MOC_HEADERS        = thread.h midigaming.h
MOC_SOURCES        = $(MOC_HEADERS:.h=.moc.cpp)

# Normal headers, without the need of Qt Moc'ing
STD_HEADERS        = event.h

# Objects files
OBJS               = $(UI_FILES:%.ui=ui_%.h) $(SOURCES:.cpp=.o) $(MOC_SOURCES:.cpp=.o)
OBJS              := $(addprefix $(DISTDIR)/, $(OBJS))

all: CXXFLAGS     += -O2
all: CXXFLAGS     += -pedantic
all: createstructure $(DISTNAME)
	$(ECHO_EXEC) Release done !

# Add DEBUG define and debug infos for gdb (-ggdb)
debug: CXXFLAGS += -DDEBUG -ggdb
debug: CXXFLAGS += -fpermissive
debug: createstructure $(DISTNAME)
	$(ECHO_EXEC) Debug done !

# Executable rule
$(DISTNAME): $(OBJS)
	$(CXX) $(INCDIRS) $^ $(LIBS) $(LIBDIR) -o $(DISTDIR)/$@ $(QT_OPT)

# Generate cpp from h via Qt's Meta Object Compiler
%.moc.cpp: %.h
	@echo Moc\'ing
	$(MOC_EXEC) $(INCDIRS) $< -o $@

# Generate ui_filename.h from filename.ui via Qt's Uic Compiler
ui_%.h: %.ui
	@echo Uic\'ing
	$(UIC_EXEC) $< -o $@

# Generate object files
%.o: %.cpp
	$(CXX) $(CXXFLAGS) $(INCDIRS) -c $< -o $@ $(QT_OPT)

createstructure:
	@echo $(OBJS)
	@echo Creating directory structure
	$(TEST_EXEC) $(DISTDIR) || $(MKDIR_EXEC) $(DISTDIR)
	$(COPY_EXEC) --parents $(SOURCES) $(DISTDIR)/
	$(COPY_EXEC) --parents $(MOC_HEADERS) $(STD_HEADERS) $(DISTDIR)/
	$(COPY_EXEC) --parents $(UI_FILES) $(DISTDIR)/

.PHONY: clean
clean:
	$(RM_EXEC) $(DISTDIR)

distclean:
	$(RM_EXEC) $(DISTDIR)/*.o $(DISTDIR)/ui_*.h *.moc.cpp $(DISTDIR)/$(DISTNAME)

